import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import request from './utils/request'
import api from './api'

import dataProce from '@/utils/dataProce.js'


// 配置 API 密钥
// const API_KEY = 'd4e96ed1d3a66152c1c213c4ead295bd.Jnq8Xqo2Z6hN65DR'


// class Utils {
// 	constructor() {
// 		this.dataProce = dataProce
// 	}
// }

const app = createApp(App)

//@todo  需要优化
const location = window.location;
const protocol = location.protocol;
const host = location.hostname;
const customOrigin = `${protocol}//${host}`;
app.config.globalProperties.$BASE_URL = customOrigin;



// 全局挂载请求方法和 API
// 设置全局 BASE_URL
app.config.globalProperties.$request = request;
app.config.globalProperties.$api = api;
// app.config.globalProperties.$apiKey = API_KEY; // 全局挂载 API 密钥

app.config.globalProperties.$util = dataProce;

const pinia = createPinia()

app.use(pinia)
app.use(router)


app.mount('#app')
