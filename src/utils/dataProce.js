
const modulesIcon = import.meta.glob('/src/assets/icon/*', { eager: true })
const modulesImgs = import.meta.glob('/src/assets/imgs/*', { eager: true })
const modulesMedia = import.meta.glob('/src/assets/media/*', { eager: true })

const urlProceIcon = (name) => {
	const path = `/src/assets/icon/${name}`
	return modulesIcon[path]?.default
}
const urlProceImgs = (name) => {
	const path = `/src/assets/imgs/${name}`
	return modulesImgs[path]?.default
}
const urlProceMedia = (name) => {
	const path = `/src/assets/media/${name}`
	return modulesMedia[path]?.default
}
export default {
	urlProceIcon,
	urlProceImgs,
	urlProceMedia
}